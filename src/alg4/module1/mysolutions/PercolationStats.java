package alg4.module1.mysolutions;

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats
{

    private static final double CONFIDENCE_95 = 1.96;
    private final double[] results;
    private final int t;

    private PercolationStats(int n, int t)
    {
        if (n < 1 || t < 1)
        {
            throw new IllegalArgumentException("Illegal Arguments..mus t by > 1");
        }

        this.t = t;
        results = new double[t];
        for (int k = 0; k < t; k++)
        {
            Percolation per = new Percolation(n);
            int openSites = 0;
            while (!per.percolates())
            {
                int i = StdRandom.uniform(1, n + 1);
                int j = StdRandom.uniform(1, n + 1);

                if (!per.isOpen(i, j))
                {
                    per.open(i, j);
                    openSites += 1;
                }
            }
            double threshold = (double) openSites / (double) (n * n);
            results[k] = threshold;
        }
    }

    public static void main(String[] args)
    {
        int n = Integer.parseInt(args[0]);
        int t = Integer.parseInt(args[1]);
        PercolationStats stats = new PercolationStats(n, t);
        StdOut.println("Mean = " + stats.mean());
        StdOut.println("Standard Deviation = " + stats.stddev());
        StdOut.println("95% Confidence Interval = " + stats.confidenceLo() + " , " + stats.confidenceHi());
    }

    private double mean()
    {
        return StdStats.mean(results);
    }

    private double stddev()
    {
        return StdStats.stddev(results);
    }

    private double confidenceLo()
    {
        return mean() - (CONFIDENCE_95 * stddev() / Math.sqrt(t));
    }

    private double confidenceHi()
    {
        return mean() + (CONFIDENCE_95 * stddev() / Math.sqrt(t));
    }

}
