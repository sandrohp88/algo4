package alg4.module1.mysolutions;

import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class Permutation
{
    public static void main(String[] args)
    {
        RandomizedQueue<String> queue = new RandomizedQueue<>();
        int k = Integer.parseInt(args[0]);
        String str;
        while(!StdIn.isEmpty()){
            str = StdIn.readString();
            queue.enqueue(str);
        }

        for (int i = 0; i < k; i++)
        {
            StdOut.println(queue.dequeue());
        }
    }
}
