package alg4.module1.mysolutions;

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {

    private Item[] q;       // queue elements
    private int n;          // number of elements on queue
    private int last;       // index of next available slot
    // construct an empty randomized queue

    RandomizedQueue() {
        q = (Item[]) new Object[2];
        n = 0;
        last = 0;
    }

    // unit testing (optional)
    public static void main(String[] args) {
        RandomizedQueue<String> queue = new RandomizedQueue<>();
        queue.enqueue("Sandro");
        queue.enqueue("Herrera");
        queue.enqueue("Pallares");
        queue.enqueue("Izada");
        queue.enqueue("Galiano");
        queue.enqueue("Lisette");

        while (queue.iterator().hasNext()) {
            StdOut.println(queue.dequeue());
        }
        StdOut.println("(" + queue.size() + " left on queue)");

    }

    // is the randomized queue empty?
    public boolean isEmpty() {
        return n == 0;
    }

    // resize the underlying array
    private void resize(int capacity) {
        assert capacity >= n;
        Item[] temp = (Item[]) new Object[capacity];
        System.arraycopy(q, 0, temp, 0, n);
        q = temp;
        last = n;
    }

    // return the number of items on the randomized queue
    public int size() {
        return n;
    }

    // add the item in a random order
    public void enqueue(Item item) {
        // double size of array if necessary and recopy to front of array
        if (n == q.length) {
            resize(2 * q.length);   // double size of array if necessary
        }
        q[last++] = item;                        // add item
        if (last == q.length) {
            last = 0;          // wrap-around
        }
        n++;

    }

    // remove and return a random item in a random order
    public Item dequeue() {

        if (isEmpty()) {
            throw new NoSuchElementException("Queue underflow");
        }
        int randomIndex = StdRandom.uniform(n);
        Item item = q[randomIndex];
        q[randomIndex] = q[size() - 1];
        // to avoid loitering
        q[size() - 1] = null;
        n--;
        //first++;
        // if (first == q.length) first = 0;           // wrap-around
        // shrink size of array if necessary
        if (n > 0 && n == q.length / 4) {
            resize(q.length / 2);
        }
        return item;
    }

    // return a random item (but do not remove it)
    public Item sample() {
        if (isEmpty()) {
            throw new NoSuchElementException("Queue underflow");
        }
        int randomIndex = StdRandom.uniform(n);
        return q[randomIndex];
    }

    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {
        return new RandomIterator();
    }

    // an iterator, doesn't implement remove() since it's optional
    private class RandomIterator implements Iterator<Item> {

        private int i = 0;
        private int[] shuffledIndex;

        RandomIterator() {
            shuffledIndex = new int[size()];
            for (int j = 0; j < shuffledIndex.length; j++) {
                shuffledIndex[j] = j;
            }
        }

        public boolean hasNext() {
            return i < n;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public Item next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            Item item = q[shuffledIndex[i]];
            i++;
            return item;
        }
    }
}
