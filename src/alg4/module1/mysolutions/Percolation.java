package alg4.module1.mysolutions;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;



public class Percolation
{

    private boolean[][] grid;
    private WeightedQuickUnionUF topVirtualSite;
    private WeightedQuickUnionUF bottomVirtualSite;
    private int openSites;
    private int topVirtualRoot;
    private int bottomVirtualRoot;
    private int N;
    // create n-by-n grid, with all sites blocked
    Percolation(int n)  {
        if (n > 0){
            grid = new boolean[n][n];
            topVirtualSite = new WeightedQuickUnionUF(n * n + 2);
            bottomVirtualSite = new WeightedQuickUnionUF(n * n + 1);
            openSites = 0;
            topVirtualRoot = n * n;
            bottomVirtualRoot = n * n + 1;
            N = n;

        }else{
            throw new java.lang.IllegalArgumentException();
        }


    }

    // test client
    public static void main(String[] args){


    }

    // open site (row, col) if it is not open already
    public    void open(int row, int col){
        validate(row, col);
        if(!isOpen(row, col)){

            grid[row-1][col-1] =true;

            if (row == 1) {
                topVirtualSite.union(col-1, topVirtualRoot);
                bottomVirtualSite.union(col-1, topVirtualRoot);
            }
            if (row == N)
            {

                topVirtualSite.union((row - 1) * N + col - 1, bottomVirtualRoot);
            }
            //down
            if (row > 1 && isOpen(row - 1, col))
            {

                topVirtualSite.union((row - 1) * N + col - 1, (row - 2) * N + col - 1);
                bottomVirtualSite.union((row - 1) * N + col - 1, (row - 2) * N + col - 1);
            }
            //up
            if (row < N && isOpen(row+1, col)) {
                topVirtualSite.union((row-1)*N+col-1, row*N+col-1);
                bottomVirtualSite.union((row-1)*N+col-1, row*N+col-1);
            }
            if (col > 1   && isOpen(row, col-1)) {
                topVirtualSite.union((row-1)*N+col-1, (row-1)*N+col-2);
                bottomVirtualSite.union((row-1)*N+col-1, (row-1)*N+col-2);
            }
            if (col < N && isOpen(row, col+1)) {
                topVirtualSite.union((row-1)*N+col-1, (row-1)*N+col);
                bottomVirtualSite.union((row-1)*N+col-1, (row-1)*N+col);
            }

            openSites++;
        }
    }

    // is site (row, col) open?
    public boolean isOpen(int row, int col){
        validate(row, col);
        return grid[row-1][col-1];
    }

    // is site (row, col) full?
    public boolean isFull(int row, int col) {
        validate(row, col);
        return bottomVirtualSite.connected((row - 1) * N + col - 1, topVirtualRoot);
    }

    // number of open sites
    public     int numberOfOpenSites() {
        return openSites;
    }

    // does the system percolate?
    public boolean percolates(){

        return topVirtualSite.connected(topVirtualRoot,bottomVirtualRoot);
    }



    // Monte Carlo simulation
    // 1- Initialize all sites to be blocked.
    // 2- Repeat the following until the system percolates:
    //      2.1- Choose a site uniformly at random among all blocked sites.
    //      2.2- Open the site
    // 3- The fraction of sites that are opened when the system percolates
    //    provides an estimate of the percolation threshold.

    // validates  row and col are in the appropriated range
    private void validate(int row, int col){
        if (row < 1 || row > N ||
                col < 1 || col > N){
            throw new java.lang.IllegalArgumentException();
        }
    }
}
