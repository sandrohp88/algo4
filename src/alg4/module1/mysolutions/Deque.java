/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alg4.module1.mysolutions;

import edu.princeton.cs.algs4.StdOut;
import java.util.Iterator;

/**
 *
 * @author sandr
 * @param <Item>
 */
public class Deque<Item> implements Iterable<Item> {

    private Node first;    // beginning of queue
    private Node last;     // end of queue
    private int n;               // number of elements on queue

    // construct an empty deque
    public Deque() {
        first = null;
        last = null;
        n = 0;
    }

    // unit testing (optional)
    public static void main(String[] args) {
        Deque<Integer> deque = new Deque<>();
        System.out.println(deque.isEmpty());
        deque.addFirst(1);
//        System.out.println(deque.removeLast());
//        System.out.println(deque.isEmpty());
//        System.out.println(deque.isEmpty());
        deque.addFirst(2);
        System.out.println(deque.removeLast());
        //deque.addFirst(7);
        //deque.addFirst(8);
        //deque.addFirst(9);
        //deque.removeLast();
        System.out.println(deque.isEmpty());
        System.out.println(deque.removeLast());

    }

    // is the deque empty?
    public boolean isEmpty() {
        return n == 0;
    }

    // return the number of items on the deque
    public int size() {
        return n;
    }

    // add the item to the front
    public void addFirst(Item item) {
        if (item == null) {
            throw new java.lang.IllegalArgumentException("Argument cannot be null");
        }
        Node oldFirst = first;
        first = new Node();
        first.item = item;
        first.next = oldFirst;
        first.previous = null; // avoid loitering
        if (last == null) {
            last = first;
        }
        n++;
    }

    // add the item to the end
    public void addLast(Item item) {
        if (item == null) {
            throw new java.lang.IllegalArgumentException("Argument must not be null");
        }
        Node oldLast = last;
        last = new Node();
        last.item = item;
        last.next = null; // avoid loitering
        last.previous = oldLast;

        if (isEmpty()) {
            first = last;
        } else {
            oldLast.next = last;
        }
        n++;

    }

    // remove and return the item from the front
    public Item removeFirst() {
        if (isEmpty()) {
            throw new java.util.NoSuchElementException("Deque underflow");
        }
        Item item = first.item;
        first = first.next;
        n--;
        if (isEmpty()) {
            last = null;   // to avoid loitering
        }
        return item;

    }

    // remove and return the item from the end
    public Item removeLast() {
        if (isEmpty()) {
            throw new java.util.NoSuchElementException("Deque underflow");
        }
        Item item = last.item;
        last = last.previous;
        n--;
        if (isEmpty()) {
            first = last;
        }
        return item;

    }

    // return an iterator over items in order from front to end
    @Override
    public Iterator<Item> iterator() {
        return new DequeIterator();

    }

    // helper linked list class
    private class Node {

        private Item item;
        private Node next;
        private Node previous;
    }

    private class DequeIterator implements Iterator<Item> {

        private Node current = first;

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public void remove() {
            throw new java.lang.UnsupportedOperationException();
        }

        @Override
        public Item next() {

            if (!hasNext()) {
                throw new java.util.NoSuchElementException();
            }

            Item item = current.item;

            current = current.next;

            return item;

        }

    }

}
